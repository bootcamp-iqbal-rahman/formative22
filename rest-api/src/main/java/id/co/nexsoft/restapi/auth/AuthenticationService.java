package id.co.nexsoft.restapi.auth;

import java.time.LocalDate;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import id.co.nexsoft.restapi.repository.UserRepository;
import id.co.nexsoft.restapi.jwt.JwtService;
import id.co.nexsoft.restapi.model.User;

@Service
public class AuthenticationService {
    @Autowired
    private UserRepository repository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private JwtService jwtService;
    @Autowired
    private AuthenticationManager authenticationManager;
    
    public AuthenticationResponse register(RegisterRequest request){
        User user = new User();
        user.setName(request.getName());
        user.setUsername(request.getUsername());
        user.setEmail(request.getEmail());
        user.setPassword(passwordEncoder.encode(request.getPassword()));
        user.setAvatar("/profile.png");
        user.setCreatedDate(LocalDate.now());
        repository.save(user);

        var jwtToken = jwtService.generateToken(user);
        AuthenticationResponse authenticationResponse = new AuthenticationResponse();
        authenticationResponse.setToken(jwtToken);

        return authenticationResponse;
    }

    public AuthenticationResponse authenticate(AuthenticationRequest request) {
        User userReq = new User();
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(request.getEmail(), request.getPassword()));
        var user = repository.findByUsernameOrEmail(request.getEmail()).orElseThrow();
        AuthenticationResponse authenticationResponse = new AuthenticationResponse();
        userReq.setUsername(request.getEmail());
        userReq.setPassword(request.getPassword());

        if (user != null) {
            var jwtToken = jwtService.generateToken(userReq);
            authenticationResponse.setToken(jwtToken);
        }

        return authenticationResponse;
    }

    public AuthenticationResponse googleSignIn(GoogleRequest request) {
        User userReq = new User();
        Optional<User> user = repository.findByUsernameOrEmail(request.getEmail());
        AuthenticationResponse authenticationResponse = new AuthenticationResponse();

        if (user.isPresent()) {
            userReq.setUsername(request.getEmail());
            userReq.setPassword(user.get().getPassword());
            var jwtToken = jwtService.generateToken(userReq);
            authenticationResponse.setToken(jwtToken);

            return authenticationResponse;
        } else {
            User userGoogle = new User();
            userGoogle.setName(request.getName());
            userGoogle.setEmail(request.getEmail());
            userGoogle.setAvatar(request.getAvatar());
            userGoogle.setCreatedDate(LocalDate.now());
            repository.save(userGoogle);

            userReq.setUsername(request.getEmail());
            userReq.setPassword(userGoogle.getPassword());
            
            var jwtToken = jwtService.generateToken(userReq);
            authenticationResponse.setToken(jwtToken);
            return authenticationResponse;
        }
    }
}