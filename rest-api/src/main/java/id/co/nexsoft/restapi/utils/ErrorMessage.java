package id.co.nexsoft.restapi.utils;

import org.hibernate.JDBCException;

public class ErrorMessage {

    public static String extractJDBCErrorMessage(JDBCException ex) {
        String rawMessage = ex.getMessage();
        int startIndex = rawMessage.indexOf("Unknown column '");
        int endIndex = rawMessage.indexOf("' in 'field list'");
        
        return rawMessage.substring(startIndex, endIndex + 1);
    }
}
