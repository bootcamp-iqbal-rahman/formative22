package id.co.nexsoft.restapi.auth;

public class RegisterRequest {
    private String name;
    private String username;
    private String email;
    private String avatar;
    private String password;
    
    public RegisterRequest() {
    }

    public RegisterRequest(String name, String username, String email, String avatar, String password) {
        this.name = name;
        this.username = username;
        this.email = email;
        this.avatar = avatar;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
