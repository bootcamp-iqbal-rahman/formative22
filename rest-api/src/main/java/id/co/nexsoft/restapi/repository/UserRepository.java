package id.co.nexsoft.restapi.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import id.co.nexsoft.restapi.model.User;

public interface UserRepository extends JpaRepository<User, Long> {
    List<User> findAllByDeletedDateIsNull();

    @Query("SELECT u FROM User u WHERE u.username = :user OR u.email = :user AND deletedDate IS NULL")
    Optional<User> findByUsernameOrEmail(@Param("user") String username);
}