package id.co.nexsoft.restapi.service;

import java.util.List;

public interface GetByName<T> {
    List<T> getDataByName(String search);
}
