package id.co.nexsoft.restapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import id.co.nexsoft.restapi.model.Ingredients;

public interface IngredientsRepository extends JpaRepository<Ingredients, Long> {
    List<Ingredients> findAllByDeletedDateIsNull();

    @Query("SELECT m FROM Ingredients m WHERE m.menuId = :id AND m.deletedDate IS NULL")
    List<Ingredients> findDataByMenu(@Param("id") Long id);
}