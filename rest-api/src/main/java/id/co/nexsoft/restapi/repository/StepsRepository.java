package id.co.nexsoft.restapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import id.co.nexsoft.restapi.model.Steps;

public interface StepsRepository extends JpaRepository<Steps, Long> {
    List<Steps> findAllByDeletedDateIsNull();

    @Query("SELECT m FROM Steps m WHERE m.menuId = :id AND m.deletedDate IS NULL")
    List<Steps> findDataByMenu(@Param("id") Long id);
}