package id.co.nexsoft.restapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import id.co.nexsoft.restapi.model.Menu;

public interface MenuRepository extends JpaRepository<Menu, Long> {
    List<Menu> findAllByDeletedDateIsNull();

    @Query("SELECT m FROM Menu m WHERE m.name LIKE %:name% AND m.deletedDate IS NULL")
    List<Menu> findDataByName(@Param("name") String name);

    @Query("SELECT m FROM Menu m WHERE m.userId = :id AND m.deletedDate IS NULL")
    List<Menu> findDataByUser(@Param("id") Long id);
}