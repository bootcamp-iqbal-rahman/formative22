package id.co.nexsoft.restapi.service.impl;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.nexsoft.restapi.model.Ingredients;
import id.co.nexsoft.restapi.repository.IngredientsRepository;
import id.co.nexsoft.restapi.service.DefaultService;
import id.co.nexsoft.restapi.service.GetByIdService;
import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import jakarta.transaction.Transactional;

@Service
public class IngredientsServiceImpl implements DefaultService<Ingredients>, GetByIdService<Ingredients> {
    @Autowired
    private IngredientsRepository repo;

    @Autowired
    private EntityManager entityManager;

    @Override
    public List<Ingredients> getAllData() {
        return repo.findAll();
    }

    @Override
    public List<Ingredients> getAllActiveData() {
        return repo.findAllByDeletedDateIsNull();
    }

    @Override
    public Optional<Ingredients> getDataById(Long id) {
        return repo.findById(id);
    }

    @Override
    public boolean deleteDataById(Long id) {
        Optional<Ingredients> Ingredients = repo.findById(id);
        if(!Ingredients.isPresent()) {
            return false;
        }
        Ingredients.get().setDeletedDate(LocalDate.now());
        return true;
    }

    @Override
    public Ingredients putData(Ingredients data, Long id) {
        Optional<Ingredients> Ingredients = repo.findById(id);
        if(!Ingredients.isPresent()) {
            return null;
        }
        data.setId(id);
        return repo.save(data);
    }

    @Override
    public Ingredients postData(Ingredients data) {
        data.setCreatedDate(LocalDate.now());
        return repo.save(data);
    }

    @Transactional
    @Override
    public void patchData(Map<String, Object> data, Long id) {
        StringBuilder queryBuilder = new StringBuilder("UPDATE ingredients SET ");

        for (Map.Entry<String, Object> entry : data.entrySet()) {
            queryBuilder.append(entry.getKey()).append(" = :").append(entry.getKey()).append(", ");
        }

        queryBuilder.delete(queryBuilder.length() - 2, queryBuilder.length());
        queryBuilder.append(" WHERE id = :id");

        Query query = entityManager.createNativeQuery(queryBuilder.toString());

        for (Map.Entry<String, Object> entry : data.entrySet()) {
            query.setParameter(entry.getKey(), entry.getValue());
        }

        query.setParameter("id", id);
        query.executeUpdate();
    }

    @Override
    public List<Ingredients> getDataByForeignId(Long id) {
        return repo.findDataByMenu(id);
    }
}