package id.co.nexsoft.restapi.service;

import java.util.List;

public interface GetByIdService<T> {
    List<T> getDataByForeignId(Long id);
}
