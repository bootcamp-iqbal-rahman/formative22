package id.co.nexsoft.restapi.service.impl;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.nexsoft.restapi.model.Menu;
import id.co.nexsoft.restapi.repository.MenuRepository;
import id.co.nexsoft.restapi.service.DefaultService;
import id.co.nexsoft.restapi.service.GetByIdService;
import id.co.nexsoft.restapi.service.GetByName;
import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import jakarta.transaction.Transactional;

@Service
public class MenuServiceImpl implements DefaultService<Menu>, GetByIdService<Menu>, GetByName<Menu> {
    @Autowired
    private MenuRepository repo;

    @Autowired
    private EntityManager entityManager;

    @Override
    public List<Menu> getAllData() {
        return repo.findAll();
    }

    @Override
    public List<Menu> getAllActiveData() {
        return repo.findAllByDeletedDateIsNull();
    }

    @Override
    public Optional<Menu> getDataById(Long id) {
        return repo.findById(id);
    }

    @Override
    public boolean deleteDataById(Long id) {
        Optional<Menu> Menu = repo.findById(id);
        if(!Menu.isPresent()) {
            return false;
        }
        Menu.get().setDeletedDate(LocalDate.now());
        return true;
    }

    @Override
    public Menu putData(Menu data, Long id) {
        Optional<Menu> Menu = repo.findById(id);
        if(!Menu.isPresent()) {
            return null;
        }
        data.setId(id);
        return repo.save(data);
    }

    @Override
    public Menu postData(Menu data) {
        data.setCreatedDate(LocalDate.now());
        return repo.save(data);
    }

    @Transactional
    @Override
    public void patchData(Map<String, Object> data, Long id) {
        StringBuilder queryBuilder = new StringBuilder("UPDATE menu SET ");

        for (Map.Entry<String, Object> entry : data.entrySet()) {
            queryBuilder.append(entry.getKey()).append(" = :").append(entry.getKey()).append(", ");
        }

        queryBuilder.delete(queryBuilder.length() - 2, queryBuilder.length());
        queryBuilder.append(" WHERE id = :id");

        Query query = entityManager.createNativeQuery(queryBuilder.toString());

        for (Map.Entry<String, Object> entry : data.entrySet()) {
            query.setParameter(entry.getKey(), entry.getValue());
        }

        query.setParameter("id", id);
        query.executeUpdate();
    }

    @Override
    public List<Menu> getDataByForeignId(Long id) {
        return repo.findDataByUser(id);
    }

    @Override
    public List<Menu> getDataByName(String search) {
        return repo.findDataByName(search);
    }
}