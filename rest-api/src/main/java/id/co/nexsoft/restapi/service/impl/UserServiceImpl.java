package id.co.nexsoft.restapi.service.impl;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.nexsoft.restapi.model.User;
import id.co.nexsoft.restapi.repository.UserRepository;
import id.co.nexsoft.restapi.service.DefaultService;
import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import jakarta.transaction.Transactional;

@Service
public class UserServiceImpl implements DefaultService<User> {
    @Autowired
    private UserRepository repo;

    @Autowired
    private EntityManager entityManager;

    @Override
    public List<User> getAllData() {
        return repo.findAll();
    }

    @Override
    public List<User> getAllActiveData() {
        return repo.findAllByDeletedDateIsNull();
    }

    @Override
    public Optional<User> getDataById(Long id) {
        return repo.findById(id);
    }

    @Override
    public boolean deleteDataById(Long id) {
        Optional<User> User = repo.findById(id);
        if(!User.isPresent()) {
            return false;
        }
        User.get().setDeletedDate(LocalDate.now());
        return true;
    }

    @Override
    public User putData(User data, Long id) {
        Optional<User> User = repo.findById(id);
        if(!User.isPresent()) {
            return null;
        }
        data.setId(id);
        return repo.save(data);
    }

    @Override
    public User postData(User data) {
        return repo.save(data);
    }

    @Transactional
    @Override
    public void patchData(Map<String, Object> data, Long id) {
        StringBuilder queryBuilder = new StringBuilder("UPDATE user SET ");

        for (Map.Entry<String, Object> entry : data.entrySet()) {
            queryBuilder.append(entry.getKey()).append(" = :").append(entry.getKey()).append(", ");
        }

        queryBuilder.delete(queryBuilder.length() - 2, queryBuilder.length());
        queryBuilder.append(" WHERE id = :id");

        Query query = entityManager.createNativeQuery(queryBuilder.toString());

        for (Map.Entry<String, Object> entry : data.entrySet()) {
            query.setParameter(entry.getKey(), entry.getValue());
        }

        query.setParameter("id", id);
        query.executeUpdate();
    }
}