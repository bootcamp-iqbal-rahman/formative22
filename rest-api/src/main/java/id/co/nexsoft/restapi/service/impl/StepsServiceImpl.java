package id.co.nexsoft.restapi.service.impl;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.nexsoft.restapi.model.Steps;
import id.co.nexsoft.restapi.repository.StepsRepository;
import id.co.nexsoft.restapi.service.DefaultService;
import id.co.nexsoft.restapi.service.GetByIdService;
import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import jakarta.transaction.Transactional;

@Service
public class StepsServiceImpl implements DefaultService<Steps>, GetByIdService<Steps> {
    @Autowired
    private StepsRepository repo;

    @Autowired
    private EntityManager entityManager;

    @Override
    public List<Steps> getAllData() {
        return repo.findAll();
    }

    @Override
    public List<Steps> getAllActiveData() {
        return repo.findAllByDeletedDateIsNull();
    }

    @Override
    public Optional<Steps> getDataById(Long id) {
        return repo.findById(id);
    }

    @Override
    public boolean deleteDataById(Long id) {
        Optional<Steps> Steps = repo.findById(id);
        if(!Steps.isPresent()) {
            return false;
        }
        Steps.get().setDeletedDate(LocalDate.now());
        return true;
    }

    @Override
    public Steps putData(Steps data, Long id) {
        Optional<Steps> Steps = repo.findById(id);
        if(!Steps.isPresent()) {
            return null;
        }
        data.setId(id);
        return repo.save(data);
    }

    @Override
    public Steps postData(Steps data) {
        data.setCreatedDate(LocalDate.now());
        return repo.save(data);
    }

    @Transactional
    @Override
    public void patchData(Map<String, Object> data, Long id) {
        StringBuilder queryBuilder = new StringBuilder("UPDATE steps SET ");

        for (Map.Entry<String, Object> entry : data.entrySet()) {
            queryBuilder.append(entry.getKey()).append(" = :").append(entry.getKey()).append(", ");
        }

        queryBuilder.delete(queryBuilder.length() - 2, queryBuilder.length());
        queryBuilder.append(" WHERE id = :id");

        Query query = entityManager.createNativeQuery(queryBuilder.toString());

        for (Map.Entry<String, Object> entry : data.entrySet()) {
            query.setParameter(entry.getKey(), entry.getValue());
        }

        query.setParameter("id", id);
        query.executeUpdate();
    }

    @Override
    public List<Steps> getDataByForeignId(Long id) {
        return repo.findDataByMenu(id);
    }
}