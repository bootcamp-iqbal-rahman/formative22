package id.co.nexsoft.restapi.controller;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.hibernate.JDBCException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.restapi.model.Menu;
import id.co.nexsoft.restapi.model.User;
import id.co.nexsoft.restapi.service.DefaultService;
import id.co.nexsoft.restapi.service.GetByIdService;
import id.co.nexsoft.restapi.service.GetByName;
import id.co.nexsoft.restapi.utils.StatusCode;
import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/data/menu")
@Validated
public class MenuController extends StatusCode {
    @Autowired
    private DefaultService<Menu> service;

    @Autowired
    private DefaultService<User> userService;

    @Autowired
    private GetByIdService<Menu> getByIdService;

    @Autowired
    private GetByName<Menu> getByNameService;
    DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern("dd MMMM yyyy");

    @GetMapping
    public ResponseEntity<?> getAllActiveData() {
        List<Menu> data = service.getAllActiveData();
        return new ResponseEntity<>(data, HttpStatus.OK);
    }

    @GetMapping("/all")
    public ResponseEntity<?> getAllData() {
        List<Menu> data = service.getAllData();
        return new ResponseEntity<>(data, HttpStatus.OK);
    }

    @GetMapping("/title")
    public ResponseEntity<?> getDataByName(@RequestParam("search") String search) {
        List<Menu> data = getByNameService.getDataByName(search);
        List<String> titleList = new ArrayList<>();
        int i = 1;
        for (Menu menu : data) {
            titleList.add(menu.getName());
            if (i == 5) {
                break;
            }
            i++;
        }
        return new ResponseEntity<>(titleList, HttpStatus.OK);
    }

    @GetMapping("/title/all")
    public ResponseEntity<?> getAllDataByName(@RequestParam("search") String search) {
        List<Menu> data = getByNameService.getDataByName(search);
        List<Map<String, Object>> returnData = new ArrayList<>();
        for (Menu menu : data) {
            Map<String, Object> object = new HashMap<>();

            object.put("id", menu.getId());
            object.put("name", menu.getName());
            object.put("description", menu.getDescription());
            object.put("image", menu.getImage());
            object.put("username", userService.getDataById(menu.getUserId()).get().getName());
            object.put("created_date", menu.getCreatedDate().format(outputFormatter));
            object.put("updated_date", menu.getUpdatedDate() == null ? null : menu.getUpdatedDate().format(outputFormatter));
            returnData.add(object);
        }
        return new ResponseEntity<>(returnData, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getDataById(@PathVariable Long id) {
        Optional<Menu> data = service.getDataById(id);
        Map<String, Object> returnData = new HashMap<>();

        if (!data.isPresent()) {
            return new ResponseEntity<>(get404(), HttpStatus.NOT_FOUND);
        } 
        returnData.put("id", data.get().getId());
        returnData.put("name", data.get().getName());
        returnData.put("description", data.get().getDescription());
        returnData.put("image", data.get().getImage());
        returnData.put("username", userService.getDataById(data.get().getUserId()).get().getName());
        returnData.put("created_date", data.get().getCreatedDate().format(outputFormatter));
        returnData.put("updated_date", data.get().getUpdatedDate() == null ? null : data.get().getUpdatedDate().format(outputFormatter));
        return new ResponseEntity<>(returnData, HttpStatus.OK);
    }

    @GetMapping("/user/{id}")
    public ResponseEntity<?> getDataByUser(@PathVariable Long id) {
        Optional<User> user = userService.getDataById(id);
        List<Menu> data = getByIdService.getDataByForeignId(id);
        return (!user.isPresent()) ? 
            new ResponseEntity<>(get404(), HttpStatus.NOT_FOUND) : 
            new ResponseEntity<>(data, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<?> postData(@Valid @RequestBody Menu data) {
        service.postData(data);
        return new ResponseEntity<>(get201(), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> putData(@Valid @RequestBody Menu data, @PathVariable Long id) {
        Menu dataPut = service.putData(data, id);
        return (dataPut == null) ?
            new ResponseEntity<>(get404(), HttpStatus.NOT_FOUND) : 
            new ResponseEntity<>(dataPut, HttpStatus.OK);
    }   

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteData(@PathVariable Long id) {
        boolean dataDelete = service.deleteDataById(id);
        return (!dataDelete) ? 
            new ResponseEntity<>(get404(), HttpStatus.NOT_FOUND) : 
            new ResponseEntity<>(get200(), HttpStatus.OK);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<?> patchData(@PathVariable Long id, @RequestBody Map<String, Object> data) {
        Optional<Menu> dataPatch = service.getDataById(id);
        try {
            if (!dataPatch.isPresent()) {
                return new ResponseEntity<>(get404(), HttpStatus.NOT_FOUND);
            }
            service.patchData(data, id);
            return new ResponseEntity<>(get200(), HttpStatus.OK);
        } catch (JDBCException e) {
            return new ResponseEntity<>(
                getCustom(CODE[4], extractJDBCErrorMessage(e)), 
                HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }
}
