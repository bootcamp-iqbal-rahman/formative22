package id.co.nexsoft.restapi.controller;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.hibernate.JDBCException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.restapi.model.Steps;
import id.co.nexsoft.restapi.model.Menu;
import id.co.nexsoft.restapi.service.DefaultService;
import id.co.nexsoft.restapi.service.GetByIdService;
import id.co.nexsoft.restapi.utils.StatusCode;
import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/data/steps")
@Validated
public class StepsController extends StatusCode {
    @Autowired
    private DefaultService<Steps> service;

    @Autowired
    private DefaultService<Menu> menuService;

    @Autowired
    private GetByIdService<Steps> getByIdService;

    @GetMapping
    public ResponseEntity<?> getAllActiveData() {
        List<Steps> data = service.getAllActiveData();
        return new ResponseEntity<>(data, HttpStatus.OK);
    }

    @GetMapping("/all")
    public ResponseEntity<?> getAllData() {
        List<Steps> data = service.getAllData();
        return new ResponseEntity<>(data, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getDataById(@PathVariable Long id) {
        Optional<Steps> data = service.getDataById(id);
        return (!data.isPresent()) ? 
            new ResponseEntity<>(get404(), HttpStatus.NOT_FOUND) : 
            new ResponseEntity<>(data, HttpStatus.OK);
    }

    @GetMapping("/menu/{id}")
    public ResponseEntity<?> getDataByMenu(@PathVariable Long id) {
        Optional<Menu> Menu = menuService.getDataById(id);
        List<Steps> data = getByIdService.getDataByForeignId(id);
        return (!Menu.isPresent()) ? 
            new ResponseEntity<>(get404(), HttpStatus.NOT_FOUND) : 
            new ResponseEntity<>(data, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<?> postData(@Valid @RequestBody Steps data) {
        service.postData(data);
        return new ResponseEntity<>(get201(), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> putData(@Valid @RequestBody Steps data, @PathVariable Long id) {
        Steps dataPut = service.putData(data, id);
        return (dataPut == null) ?
            new ResponseEntity<>(get404(), HttpStatus.NOT_FOUND) : 
            new ResponseEntity<>(dataPut, HttpStatus.OK);
    }   

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteData(@PathVariable Long id) {
        boolean dataDelete = service.deleteDataById(id);
        return (!dataDelete) ? 
            new ResponseEntity<>(get404(), HttpStatus.NOT_FOUND) : 
            new ResponseEntity<>(get200(), HttpStatus.OK);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<?> patchData(@PathVariable Long id, @RequestBody Map<String, Object> data) {
        Optional<Steps> dataPatch = service.getDataById(id);
        try {
            if (!dataPatch.isPresent()) {
                return new ResponseEntity<>(get404(), HttpStatus.NOT_FOUND);
            }
            service.patchData(data, id);
            return new ResponseEntity<>(get200(), HttpStatus.OK);
        } catch (JDBCException e) {
            return new ResponseEntity<>(
                getCustom(CODE[4], extractJDBCErrorMessage(e)), 
                HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }
}
