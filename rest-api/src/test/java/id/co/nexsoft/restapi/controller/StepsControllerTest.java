package id.co.nexsoft.restapi.controller;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import id.co.nexsoft.restapi.model.Steps;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class StepsControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    void getAllActiveData() throws Exception {
        this.mockMvc.perform(get("/api/data/steps"))
            .andDo(print())
            .andExpect(status().isOk());
    }

    @Test
    void getAllData() throws Exception {
        this.mockMvc.perform(get("/api/data/steps/all"))
            .andDo(print())
            .andExpect(status().isOk());
    }

    @Test
    void getAllDataByMenu() throws Exception {
        this.mockMvc.perform(get("/api/data/steps/menu/{id}", 1))
            .andDo(print())
            .andExpect(status().isOk());
    }

    @Test
    void getAllDataByNullMenu() throws Exception {
        this.mockMvc.perform(get("/api/data/steps/menu/{id}", 100))
            .andDo(print())
            .andExpect(status().isNotFound());
    }

    @Test
    void getDataById() throws Exception {
        this.mockMvc.perform(get("/api/data/steps/{id}", 1))
            .andDo(print())
            .andExpect(status().isOk());
    }

    @Test
    void getDataByNullId() throws Exception {
        this.mockMvc.perform(get("/api/data/steps/{id}", 100))
            .andDo(print())
            .andExpect(status().isNotFound());
    }

    @Test
    void postData() throws Exception {
        Steps mockSteps = new Steps(
            "Step Test", "Step Desc", 1l, LocalDate.now(), null
        );

        mockMvc.perform(post("/api/data/steps")
            .contentType(MediaType.APPLICATION_JSON)
            .content(asJsonString(mockSteps))
        ).andExpect(status().isCreated());
    }

    @Test
    void postErrorData() throws Exception {
        Map<String, Object> mockSteps = new HashMap<>();
        mockSteps.put("description", "Test Post");

        mockMvc.perform(post("/api/data/steps")
            .contentType(MediaType.APPLICATION_JSON)
            .content(asJsonString(mockSteps))
        ).andExpect(status().isBadRequest());
    }

    @Test
    void putData() throws Exception {
        Steps mockSteps = new Steps(
            "Step Test", "Step Desc", 1L, LocalDate.now(), null
        );

        mockMvc.perform(put("/api/data/steps/{id}", 1)
            .contentType(MediaType.APPLICATION_JSON)
            .content(asJsonString(mockSteps))
        ).andExpect(status().isOk());
    }

    @Test
    void patchData() throws Exception {
        Map<String, Object> mockSteps = new HashMap<>();
        mockSteps.put("name", "test patch");

        mockMvc.perform(patch("/api/data/steps/{id}", 1)
            .contentType(MediaType.APPLICATION_JSON)
            .content(asJsonString(mockSteps))
        ).andExpect(status().isOk());
    }

    @Test
    void patchNullData() throws Exception {
        Map<String, Object> mockSteps = new HashMap<>();
        mockSteps.put("titles", "test patch");

        mockMvc.perform(patch("/api/data/steps/{id}", 1)
            .contentType(MediaType.APPLICATION_JSON)
            .content(asJsonString(mockSteps))
        ).andExpect(status().isUnprocessableEntity());
    }

    @Test
    void patchNullId() throws Exception {
        Map<String, Object> mockSteps = new HashMap<>();
        mockSteps.put("title", "test patch");

        mockMvc.perform(patch("/api/data/steps/{id}", 100)
            .contentType(MediaType.APPLICATION_JSON)
            .content(asJsonString(mockSteps))
        ).andExpect(status().isNotFound());
    }

    @Test
    void putDataNullId() throws Exception {
        Steps mockSteps = new Steps(
            "Step Test", "Step Desc", 1L, LocalDate.now(), null
        );

        mockMvc.perform(put("/api/data/steps/1000")
            .contentType(MediaType.APPLICATION_JSON)
            .content(asJsonString(mockSteps))
        ).andExpect(status().isNotFound());
    }

    @Test
    void deleteData() throws Exception {
        mockMvc.perform(delete("/api/data/steps/{id}", 3)
                ).andExpect(status().isOk());
    }

    @Test
    void deleteNullData() throws Exception {
        mockMvc.perform(delete("/api/data/steps/{id}", 1000)
                ).andExpect(status().isNotFound());
    }

    private String asJsonString(Object object) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        return objectMapper.writeValueAsString(object);
    }
}
