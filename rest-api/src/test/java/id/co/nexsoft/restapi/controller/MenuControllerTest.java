package id.co.nexsoft.restapi.controller;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import id.co.nexsoft.restapi.model.Menu;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class MenuControllerTest {
        @Autowired
    private MockMvc mockMvc;

    @Test
    void getAllActiveData() throws Exception {
        this.mockMvc.perform(get("/api/data/menu"))
            .andDo(print())
            .andExpect(status().isOk());
    }

    @Test
    void getAllData() throws Exception {
        this.mockMvc.perform(get("/api/data/menu/all"))
            .andDo(print())
            .andExpect(status().isOk());
    }

    @Test
    void getAllDataByUser() throws Exception {
        this.mockMvc.perform(get("/api/data/menu/user/{id}", 1))
            .andDo(print())
            .andExpect(status().isOk());
    }

    @Test
    void getAllDataByNullMenu() throws Exception {
        this.mockMvc.perform(get("/api/data/menu/user/{id}", 100))
            .andDo(print())
            .andExpect(status().isNotFound());
    }

    @Test
    void getDataById() throws Exception {
        this.mockMvc.perform(get("/api/data/menu/{id}", 1))
            .andDo(print())
            .andExpect(status().isOk());
    }

    @Test
    void getDataByNullId() throws Exception {
        this.mockMvc.perform(get("/api/data/menu/{id}", 100))
            .andDo(print())
            .andExpect(status().isNotFound());
    }

    @Test
    void postData() throws Exception {
        Menu mockMenu = new Menu(
            "Menu test", "Menu desc", 1L, null, LocalDate.now(), null, null
        );

        mockMvc.perform(post("/api/data/menu")
            .contentType(MediaType.APPLICATION_JSON)
            .content(asJsonString(mockMenu))
        ).andExpect(status().isCreated());
    }

    @Test
    void postErrorData() throws Exception {
        Map<String, Object> mockMenu = new HashMap<>();
        mockMenu.put("description", "Test Post");

        mockMvc.perform(post("/api/data/menu")
            .contentType(MediaType.APPLICATION_JSON)
            .content(asJsonString(mockMenu))
        ).andExpect(status().isBadRequest());
    }

    @Test
    void putData() throws Exception {
        Menu mockMenu = new Menu(
            "Menu Put", "Menu desc", 1L, null, LocalDate.now(), null, null
        );

        mockMvc.perform(put("/api/data/menu/{id}", 1)
            .contentType(MediaType.APPLICATION_JSON)
            .content(asJsonString(mockMenu))
        ).andExpect(status().isOk());
    }

    @Test
    void patchData() throws Exception {
        Map<String, Object> mockMenu = new HashMap<>();
        mockMenu.put("name", "test patch");

        mockMvc.perform(patch("/api/data/menu/{id}", 1)
            .contentType(MediaType.APPLICATION_JSON)
            .content(asJsonString(mockMenu))
        ).andExpect(status().isOk());
    }

    @Test
    void patchNullData() throws Exception {
        Map<String, Object> mockMenu = new HashMap<>();
        mockMenu.put("titles", "test patch");

        mockMvc.perform(patch("/api/data/menu/{id}", 1)
            .contentType(MediaType.APPLICATION_JSON)
            .content(asJsonString(mockMenu))
        ).andExpect(status().isUnprocessableEntity());
    }

    @Test
    void patchNullId() throws Exception {
        Map<String, Object> mockMenu = new HashMap<>();
        mockMenu.put("title", "test patch");

        mockMvc.perform(patch("/api/data/menu/{id}", 100)
            .contentType(MediaType.APPLICATION_JSON)
            .content(asJsonString(mockMenu))
        ).andExpect(status().isNotFound());
    }

    @Test
    void putDataNullId() throws Exception {
        Menu mockMenu = new Menu(
            "Menu test", "Menu desc", 1L, null, LocalDate.now(), null, null
        );

        mockMvc.perform(put("/api/data/menu/1000")
            .contentType(MediaType.APPLICATION_JSON)
            .content(asJsonString(mockMenu))
        ).andExpect(status().isNotFound());
    }

    @Test
    void deleteData() throws Exception {
        mockMvc.perform(delete("/api/data/menu/{id}", 3)
                ).andExpect(status().isOk());
    }

    @Test
    void deleteNullData() throws Exception {
        mockMvc.perform(delete("/api/data/menu/{id}", 1000)
                ).andExpect(status().isNotFound());
    }

    private String asJsonString(Object object) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        return objectMapper.writeValueAsString(object);
    }
}
