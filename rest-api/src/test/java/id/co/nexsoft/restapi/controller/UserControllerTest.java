package id.co.nexsoft.restapi.controller;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import id.co.nexsoft.restapi.model.User;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerTest {
    @Autowired
    private MockMvc mockMvc;

    private final String TOKEN = "";

    @Test
    void getAllActiveData() throws Exception {
        this.mockMvc.perform(get("/api/data/user"))
            .andDo(print())
            .andExpect(status().isOk());
    }

    @Test
    void getAllData() throws Exception {
        this.mockMvc.perform(get("/api/data/user/all"))
            .andDo(print())
            .andExpect(status().isOk());
    }

    @Test
    void getDataById() throws Exception {
        this.mockMvc.perform(get("/api/data/user/{id}", 1))
            .andDo(print())
            .andExpect(status().isOk());
    }

    @Test
    void getDataByNullId() throws Exception {
        this.mockMvc.perform(get("/api/data/user/{id}", 100))
            .andDo(print())
            .andExpect(status().isNotFound());
    }

    @Test
    void postData() throws Exception {
        User mockUser = new User(
            "Test name", "TestUsername", "TestEmail@gmail.com", 
            "TestPassword", "/profile.png", LocalDate.now(), null
        );

        mockMvc.perform(post("/api/data/user")
            .header("authorization", "Bearer " + TOKEN)
            .contentType(MediaType.APPLICATION_JSON)
            .content(asJsonString(mockUser))
        ).andExpect(status().isCreated());
    }

    @Test
    void postForbiddenData() throws Exception {
        Map<String, Object> mockUser = new HashMap<>();
        mockUser.put("name", "Test Post");

        mockMvc.perform(post("/api/data/user")
            .contentType(MediaType.APPLICATION_JSON)
            .content(asJsonString(mockUser))
        ).andExpect(status().isForbidden());
    }

    @Test
    void postErrorData() throws Exception {
        Map<String, Object> mockUser = new HashMap<>();
        mockUser.put("name", "Test Post");

        mockMvc.perform(post("/api/data/user")
            .contentType(MediaType.APPLICATION_JSON)
            .content(asJsonString(mockUser))
        ).andExpect(status().isBadRequest());
    }

    @Test
    void putData() throws Exception {
        User mockUser = new User(
            "Test put", "TestUsername", "TestEmail@gmail.com", 
            "TestPassword", "/profile.png", LocalDate.now(), null
        );

        mockMvc.perform(put("/api/data/user/{id}", 1)
            .contentType(MediaType.APPLICATION_JSON)
            .content(asJsonString(mockUser))
        ).andExpect(status().isOk());
    }

    @Test
    void patchData() throws Exception {
        Map<String, Object> mockUser = new HashMap<>();
        mockUser.put("name", "test patch");

        mockMvc.perform(patch("/api/data/user/{id}", 1)
            .contentType(MediaType.APPLICATION_JSON)
            .content(asJsonString(mockUser))
        ).andExpect(status().isOk());
    }

    @Test
    void patchNullData() throws Exception {
        Map<String, Object> mockUser = new HashMap<>();
        mockUser.put("titles", "test patch");

        mockMvc.perform(patch("/api/data/user/{id}", 1)
            .contentType(MediaType.APPLICATION_JSON)
            .content(asJsonString(mockUser))
        ).andExpect(status().isUnprocessableEntity());
    }

    @Test
    void patchNullId() throws Exception {
        Map<String, Object> mockUser = new HashMap<>();
        mockUser.put("title", "test patch");

        mockMvc.perform(patch("/api/data/user/{id}", 100)
            .contentType(MediaType.APPLICATION_JSON)
            .content(asJsonString(mockUser))
        ).andExpect(status().isNotFound());
    }

    @Test
    void putDataNullId() throws Exception {
        User mockUser = new User(
            "Test name", "TestUsername", "TestEmail@gmail.com", 
            "TestPassword", "/profile.png", LocalDate.now(), null
        );

        mockMvc.perform(put("/api/data/user/1000")
            .contentType(MediaType.APPLICATION_JSON)
            .content(asJsonString(mockUser))
        ).andExpect(status().isNotFound());
    }

    @Test
    void deleteData() throws Exception {
        mockMvc.perform(delete("/api/data/user/{id}", 4)
                ).andExpect(status().isOk());
    }

    @Test
    void deleteNullData() throws Exception {
        mockMvc.perform(delete("/api/data/user/{id}", 1000)
                ).andExpect(status().isNotFound());
    }

    private String asJsonString(Object object) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        return objectMapper.writeValueAsString(object);
    }
}
