/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./index.html",
    "./src/**/*.{js,ts,jsx,tsx}",
    'node_modules/flowbite-react/lib/esm/**/*.js'
  ],
  theme: {
    extend: {
      colors: {
        '1': '#FFCDB2',
        '2': '#FFB4A2',
        '3': '#E5989B',
        '4': '#B5838D',
        '5': '#6D6875',
        'background': '#F0F0F0'
      },
    },
  },
  plugins: [
    require('flowbite/plugin')
  ],
}

