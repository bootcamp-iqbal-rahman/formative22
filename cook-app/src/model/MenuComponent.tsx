interface MenuComponent {
    id: number;
    name: string;
    description: string;
    image: string;
    username: string;
    created_date: string;
    updated_date: string;
}
