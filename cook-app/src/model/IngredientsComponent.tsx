interface IngredientsComponent {
    name: string;
    amount: number;
    unit: string;
    menuId: number;
}