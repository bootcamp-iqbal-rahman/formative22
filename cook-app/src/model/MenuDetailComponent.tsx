interface MenuDetailComponent {
    id: number;
    name: string;
    description: string;
    image: string;
    username: string;
    created_date: string;
    updated_date: string;
    deleted_date: string;
}
