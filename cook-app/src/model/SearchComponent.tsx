interface Search {
    value: string;
    onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
}