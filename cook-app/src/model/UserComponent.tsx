interface UserComponent {
    id: number;
    name: string;
    avatar: string;
}