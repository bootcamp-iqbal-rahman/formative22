interface CardComponent {
    id: number;
    image: string;
    title: string;
    ingredients: string;
    avatar: string;
    username: string;
    date: string;
}