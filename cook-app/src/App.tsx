import { HomePage } from "./components/page/Home"
import { DetailPage } from "./components/page/Detail"
import { LoginPage } from "./components/page/LoginPage"
import { UserPage } from "./components/page/User"
import { NavbarTop } from "./components/atoms/Navbar"
import { Footer } from "./components/atoms/Footer"

import { BrowserRouter as Router, Route, Navigate, Routes } from "react-router-dom";
import './App.css'
import { UserAdd } from "./components/page/AddMenu"
import { UserAddIngredients } from "./components/page/AddIngredients"
import { UserMenu } from "./components/page/UserMenu"
import { UserAddSteps } from "./components/page/AddSteps"

function App() {
  const token = localStorage.getItem("token");
  const isAuthenticated = !!token;

  return (
    <>
      <div className="w-screen m-0 p-0 bg-background ">
        <NavbarTop />
          <Router>
            <Routes>
              <Route path="/" element={<HomePage />} />
              <Route path="/detail/:id" element={<DetailPage />} />
              <Route path="/login" element={isAuthenticated ? <Navigate to="/user" /> : <LoginPage />} />
              <Route path="/user" element={isAuthenticated? <UserPage />  : <Navigate to="/login" />} />
              <Route path="/user/menu" element={isAuthenticated? <UserMenu />  : <Navigate to="/login" />} />
              <Route path="/user/add" element={isAuthenticated? <UserAdd />  : <Navigate to="/login" />} />
              <Route path="/user/add/ingredients/:id" element={isAuthenticated? <UserAddIngredients />  : <Navigate to="/login" />} />
              <Route path="/user/add/steps/:id" element={isAuthenticated? <UserAddSteps />  : <Navigate to="/login" />} />
              <Route path="*" element={<Navigate to="/" />} />
            </Routes>
          </Router>
        <Footer />
      </div>
    </>
  )
}

export default App
