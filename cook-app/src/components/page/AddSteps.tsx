import React, { useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import InputField from "../atoms/InputRequired";
import { Button } from "../atoms/Button";
import { ButtonUser } from "../atoms/ButtonUser";
import axios from "axios";

export const UserAddSteps: React.FC = () => {
    const {id} = useParams();
    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const navigate = useNavigate();

    const handleMenu = async (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        const headers = { 
            'Content-Type': 'application/json', 
            'Authorization': 'Bearer ' + localStorage.getItem("token"),
        }
        try {
            const formData = {
                name: name,
                description: description,
                menuId: id
            }

            await axios.post('http://localhost:8080/api/data/steps',
                formData, { headers }
            );

            navigate("/user/add/steps/" + id);
        } catch (error) {
            
        }
    }

    const handleBack = () => {
        navigate("/user/add/ingredients/" + id)
    }

    const handleFinish = () => {
        navigate("/user/menu")
    }

    return (
        <div className="flex justify-center mt-10">
            <form
                onSubmit={handleMenu}
                >
                <InputField 
                    id="name"
                    text="Steps name" 
                    type="text"
                    placeholder="Input steps here"
                    value={name}
                    onChange={(e) => setName(e.target.value)}
                />
                <InputField 
                    id="description"
                    text="Description" 
                    type="text"
                    placeholder="Input description"
                    value={description}
                    onChange={(e) => setDescription(e.target.value)}
                />
                <Button type="submit" text="Add Steps"/>
                <div className="flex mt-4 space-x-10">
                    <ButtonUser 
                        onClick={handleBack} 
                        text="Add Ingredients"/>
                    <ButtonUser 
                        onClick={handleFinish} 
                        text="Back to Menu list"/>
                </div>
            </form>
        </div>
    );
}