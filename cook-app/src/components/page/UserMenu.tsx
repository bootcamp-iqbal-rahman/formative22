import React, { MouseEventHandler, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import axios from "axios";

export const UserMenu: React.FC = () => {
    const userId = 3;
    const [result, setResult] = useState<MenuDetailComponent[]>([]);
    const navigate = useNavigate();

    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await axios.get('http://localhost:8080/api/data/menu/user/' + userId);
                setResult(response.data);
            } catch (error) {
                
            }
        }
        
        fetchData();
    }, [])

    const handleMenu = (id: number): MouseEventHandler<HTMLDivElement> => {
        return (e) => {
            e.preventDefault();
            navigate("/user/add/ingredients/" + id)
        }
    }

    return (
        <div className="m-8 mt-10">
            {result.map((item) => (
                <div 
                    onClick={handleMenu(item.id)}
                    className="cursor-pointer bg-3 rounded-lg shadow-lg mt-3 text-left" key={item.id}>
                    <div>
                        <img src={item.image}></img>
                    </div>
                    <div className="p-6">
                        <h1 className="font-semibold">{item.name}</h1>
                        <p>{item.description}</p>
                        <p>{item.username}</p>
                        <p>{item.created_date}</p>
                    </div>
                </div>
            ))}
            <div>

            </div>
        </div>
    );
}