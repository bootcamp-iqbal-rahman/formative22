import React, { useState } from "react";
import InputField from "../atoms/InputRequired";
import { useNavigate } from "react-router-dom";
import { Button } from "../atoms/Button";
import { SignUp } from "../molecules/SignUp";
import axios from "axios";

export const LoginPage: React.FC = () => {
    const [username, setUsername] = useState<string>("");
    const [password, setPassword] = useState<string>("");
    const [errorMessage, setError] = useState<string>("");
    const navigate = useNavigate();

    const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        try {
            const requestBody = {
                email: username,
                password: password
            }

            const response = await axios.post('http://localhost:8080/api/v1/auth/authenticate',
                requestBody
            );

            const data = response.data.token;
            localStorage.setItem('token', data);
            navigate('/')
        } catch (error) {
            setError("Username or password incorrect");
        }
    };

    return (
        <div className="flex justify-center mt-8">
            <div
                className="shadow-lg rounded-md box-content p-10 max-w-80">
                <h2 className="text-xl font-semibold mb-4">Login Form</h2>
                <div className="text-left text-red-500">
                    <h5>{errorMessage}</h5>
                </div>
                <form
                    onSubmit={handleSubmit}
                    className="max-w-sm mx-auto"
                >
                    <InputField
                        id="email-login"
                        text="Username or Email"
                        type="text"
                        placeholder="Input your username or email"
                        value={username}
                        onChange={(e) => setUsername(e.target.value)}
                    />
                    <InputField
                        id="password-login"
                        text="Password"
                        type="password"
                        placeholder="Input your password"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                    />
                    <Button
                        type="submit"
                        text="Login"
                    />
                </form>
                <SignUp />
            </div>
        </div>
    );
}