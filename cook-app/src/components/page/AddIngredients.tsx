import React, { useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import InputField from "../atoms/InputRequired";
import { Button } from "../atoms/Button";
import axios from "axios";
import { ButtonUser } from "../atoms/ButtonUser";

export const UserAddIngredients: React.FC = () => {
    const {id} = useParams();
    const [name, setName] = useState("");
    const [amount, setAmount] = useState("");
    const [type, setType] = useState("");
    const navigate = useNavigate();

    const handleMenu = async (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        const headers = { 
            'Content-Type': 'application/json', 
            'Authorization': 'Bearer ' + localStorage.getItem("token"),
        }
        try {
            const formData = {
                name: name,
                amount: amount,
                unit: type,
                menuId: id
            }

            await axios.post('http://localhost:8080/api/data/ingredients',
                formData, { headers }
            );

            setName("");
            setAmount("");
            setType("");
            navigate("/user/add/ingredients/" + id);
        } catch (error) {
            
        }
    }

    const handleBack = () => {
        navigate("/user/add/steps/" + id)
    }

    const handleFinish = () => {
        navigate("/user/menu")
    }

    return (
        <div className="flex justify-center mt-10">
            <form
                onSubmit={handleMenu}
                >
                <InputField 
                    id="name"
                    text="Ingredients name" 
                    type="text"
                    placeholder="Input ingredients name"
                    value={name}
                    onChange={(e) => setName(e.target.value)}
                />
                <InputField 
                    id="amount"
                    text="Amount" 
                    type="text"
                    placeholder="Input amount"
                    value={amount}
                    onChange={(e) => setAmount(e.target.value)}
                />
                <InputField 
                    id="unit"
                    text="Type Unit" 
                    type="text"
                    placeholder="Input type"
                    value={type}
                    onChange={(e) => setType(e.target.value)}
                />
                <Button type="submit" text="Add Ingredients"/>
                <div className="flex mt-4 space-x-10">
                    <ButtonUser 
                        onClick={handleFinish} 
                        text="Back to Menu list"/>
                    <ButtonUser 
                        onClick={handleBack} 
                        text="Add Steps"/>
                </div>
            </form>
        </div>
    );
}