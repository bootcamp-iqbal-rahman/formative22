import React, { useEffect, useState } from "react";
import { LeftBody } from "../organism/DetailLeftBody"
import { RightBody } from "../organism/DetailRightBody"
import { useParams } from 'react-router-dom';
import axios from "axios";

export const DetailPage: React.FC = () => {
    const { id } = useParams();
    const [menu, setMenu] = useState<MenuComponent>();
    const [ingredients, setIngredient] = useState<IngredientsComponent[]>([]);
    const [steps, setStep] = useState<StepsComponent[]>([]);

    useEffect(() => {
        const fetchProducts = async () => {
            const { data } = await axios.get("http://localhost:8080/api/data/menu/" + id);
            setMenu(data);
        };

        const fetchIngredients = async () => {
            const { data } = await axios.get("http://localhost:8080/api/data/ingredients/menu/" + id);
            setIngredient(data);
        };

        const fetchSteps = async () => {
            const { data } = await axios.get("http://localhost:8080/api/data/steps/menu/" + id);
            setStep(data);
        };

        fetchProducts();
        fetchIngredients();
        fetchSteps();
    }, []); 

    return (
        <>
            {menu != null && (
                <>
                    <div
                        className="mx-auto h-80 overflow-y-scroll bg-cover bg-fixed bg-center bg-no-repeat shadow-lg"
                        style={{ backgroundImage: `url(${menu.image})` }}>
                    </div>
                    <div className="grid grid-cols-1 sm:grid-cols-3 sm:gap-x-5 md:gap-x-11 lg:gap-x-16">
                        <LeftBody 
                            menu={menu}
                            ingredients={ingredients}
                            steps={steps}
                        />
                        <RightBody />
                    </div>
                </>
            )}
        </>
    );
}