import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import InputField from "../atoms/InputRequired";
import { Button } from "../atoms/Button";
import axios from "axios";

export const UserAdd: React.FC = () => {
    const [name, setName] = useState("");
    const [description, setDesc] = useState("");
    const [image, setImage] = useState("");
    const navigate = useNavigate();

    const handleMenu = async (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        const headers = { 
            'Content-Type': 'application/json', 
            'Authorization': 'Bearer ' + localStorage.getItem("token"),
        }
        try {
            const formData = {
                name: name,
                description: description,
                image: image,
                userId: 3
            }

            await axios.post('http://localhost:8080/api/data/menu',
                formData, { headers }
            );

            navigate("/user/menu");
        } catch (error) {
            
        }
    }

    return (
        <div className="flex justify-center mt-10">
            <form
                onSubmit={handleMenu}
                >
                <InputField 
                    id="name"
                    text="Menu or recipe name" 
                    type="text"
                    placeholder="Input menu name or recipe name"
                    value={name}
                    onChange={(e) => setName(e.target.value)}
                />
                <InputField 
                    id="description"
                    text="Description" 
                    type="text"
                    placeholder="Input description"
                    value={description}
                    onChange={(e) => setDesc(e.target.value)}
                />
                <InputField 
                    id="image"
                    text="Image" 
                    type="text"
                    placeholder="Input image url"
                    value={image}
                    onChange={(e) => setImage(e.target.value)}
                />
                <Button type="submit" text="Add menu"/>
            </form>
        </div>
    );
}