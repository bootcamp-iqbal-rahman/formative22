import React from "react";
import { LeftBody } from "../organism/LeftBody"
import { RightBody } from "../organism/RightBody"

export const HomePage: React.FC = () => {
    return (
        <>
            <div className="grid grid-cols-1 sm:grid-cols-3 sm:gap-x-5 md:gap-x-11 lg:gap-x-16">
                <LeftBody />
                <RightBody />
            </div>
        </>
    );
}