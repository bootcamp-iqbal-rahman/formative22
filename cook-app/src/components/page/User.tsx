import React from "react";
import { ButtonUser } from "../atoms/ButtonUser";
import { useNavigate } from "react-router-dom";

export const UserPage: React.FC = () => {
    const navigate = useNavigate();

    const addMenu = () => {
        navigate("/user/add");
    }

    const updateMenu = () => {
        navigate("/user/menu")
    }

    const deleteMenu = () => {

    }

    const logout = () => {
        localStorage.removeItem('token');
        // localStorage.removeItem('data');
        navigate("/");
    }

    return (
        <>
            <div className="flex mt-10 p-6 space-x-6">
                <ButtonUser 
                    text="Add Menu"
                    onClick={addMenu}
                />
                <ButtonUser 
                    text="Update Menu"
                    onClick={updateMenu}
                />
                <ButtonUser 
                    text="Delete Menu"
                    onClick={deleteMenu}
                />
            </div>
            <div className="flex p-6 space-x-6">
                <ButtonUser 
                    text="Logout"
                    onClick={logout}
                />
            </div>
        </>
    );
}