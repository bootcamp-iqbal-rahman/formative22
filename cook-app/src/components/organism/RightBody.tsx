import { MenuList } from "../molecules/MenuList";

export const RightBody: React.FC = () => {
    return (
        <div className="col-span-1 px-8 sm:pl-0 lg:pr-20 pt-8 md:pt-14">
            <MenuList />
        </div>
    );
}