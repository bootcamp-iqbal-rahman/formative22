import { DetailCard } from "../atoms/DetailCard";
import { DetailInfo } from "../atoms/DetailInfo";

interface Component {
    menu: MenuComponent;
    ingredients: IngredientsComponent[];
    steps: StepsComponent[];
}

export const LeftBody: React.FC<Component> = (props) => {
    return (
        <div className="col-span-2 px-8 sm:pr-0 lg:pl-20 pt-8 md:pt-14">
            <DetailCard 
                name={props.menu.name}
                description={props.menu.description}
                username={props.menu.username}
                date={props.menu.created_date}
            />
            <DetailInfo 
                title="Bahan - bahan"
                ingredients={props.ingredients}
            />
            <DetailInfo 
                title="Langkah - langkah"
                ingredients={props.steps}
            />
        </div>
    );
}