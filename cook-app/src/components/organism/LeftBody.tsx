import { useState, useEffect } from "react";
import { SearchBar } from "../atoms/Search";
import { CardList } from "../molecules/CardList";
import axios from "axios";
import { SearchList } from "../molecules/SearchList";

export const LeftBody: React.FC = () => {
    const [query, setQuery] = useState("");
    const [menuQuery, setMenuQuery] = useState("");
    const [products, setProducts] = useState<string[]>([]);
    const [menuList, setMenu] = useState<MenuComponent[]>([]);

    useEffect(() => {
        const fetchProducts = async () => {
            const search = query.replace(" ", "+")
            const { data } = await axios.get("http://localhost:8080/api/data/menu/title?search=" + search);
            setProducts(data);
        };

        fetchProducts();
    }, [query]);

    useEffect(() => {
        const fetchAllMenu = async () => {
            const search = query.replace(" ", "+")
            const { data } = await axios.get("http://localhost:8080/api/data/menu/title/all?search=" + search);
            setMenu(data);
        };

        fetchAllMenu();
    }, [menuQuery]);

    const handleQueryChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setQuery(event.target.value);
    };

    const handleSubmitQuery = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        setMenuQuery(query);
    };
    
    const handleItemClick = (name: string) => {
        setQuery(name);
        setMenuQuery(name);
    }

    return (
        <div className="col-span-2 px-8 sm:pr-0 lg:pl-20 pt-8 md:pt-14">
            <form onSubmit={handleSubmitQuery}>
                <SearchBar
                    value={query}
                    onChange={handleQueryChange}
                />
            </form>
            {query !== "" && products.length > 0 && (
                <SearchList 
                    name={products}   
                    onClick={handleItemClick}               
                />
            )}
            <CardList 
                item={menuList}                        
            />
        </div>
    );
}