import { useEffect, useState } from "react";
import { Card } from "../atoms/Card";
import { PaginationButton } from "../atoms/PaginationButton";

type CardListProps = {
    item: MenuComponent[];
};

export const CardList: React.FC<CardListProps> = (props) => {
    const itemsPerPage = 5;
    const totalPages = Math.ceil(props.item.length / itemsPerPage);
    const [currentPage, setCurrentPage] = useState(1);

    useEffect(() => {
        setCurrentPage(1);
    }, [props.item]);

    const handleClickNext = () => {
        setCurrentPage(currentPage + 1);
    };

    const handleClickPrev = () => {
        setCurrentPage(currentPage - 1);
    };

    const renderDataForPage = () => {
        const startIndex = (currentPage - 1) * itemsPerPage;
        const endIndex = startIndex + itemsPerPage;
        return props.item.slice(startIndex, endIndex);
    };

    return (
        <div>
            {renderDataForPage().map((item, index) => (
                <Card
                    key={index}
                    id={item.id}
                    image={item.image}
                    title={item.name}
                    ingredients={item.description}
                    avatar={item.name}
                    username={item.username}
                    date={item.updated_date ? item.updated_date : item.created_date}
                />
            ))}
            <div className="mt-6 space-x-5 font-semibold">
                <PaginationButton 
                    onClick={handleClickPrev}
                    currentPage={currentPage}
                    maxPages={1}
                    text="Previous"
                />
                <span>Page {currentPage} of {totalPages}</span>
                <PaginationButton 
                    onClick={handleClickNext}
                    currentPage={currentPage}
                    maxPages={totalPages}
                    text="Next"
                />
            </div>
        </div>
    );
}