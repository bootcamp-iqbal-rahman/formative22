type ProductListProps = {
    name: string[];
    onClick: (list: string) => void;
};

export const SearchList: React.FC<ProductListProps> = (props) => {

    return (
        <div className="bg-3 overflow-y-scroll rounded-3xl mt-1 shadow-lg">
            {props.name.map((list, index) => (
                <div
                    key={index}
                    id={list}
                    className="text-black py-2 px-4 flex items-center justify-between gap-8 hover:bg-4 cursor-pointer"
                    onClick={() => props.onClick(list)}
                >
                    <p className="font-medium">{list}</p>
                </div>
            ))}
        </div>
    );
}