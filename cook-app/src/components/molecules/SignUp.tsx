import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { useGoogleLogin } from '@react-oauth/google';
import { FcGoogle } from "react-icons/fc";
import axios from 'axios';

interface UserToken {
    access_token: string;
}

export const SignUp: React.FC = () => {
    const [user, setUser] = useState<UserToken | null>(null);
    const navigate = useNavigate();

    const handleRegisterClick = () => {
        navigate("/register");
    };

    const handleForgetClick = () => {
        navigate("/forget");
    };

    const login = useGoogleLogin({
        onSuccess: (codeResponse) => setUser(codeResponse),
        onError: (error) => console.log('Login Failed:', error)
    });

    useEffect(() => {
        if (user) {
            axios
                .get(`https://www.googleapis.com/oauth2/v1/userinfo?access_token=${user.access_token}`, {
                    headers: {
                        Authorization: `Bearer ${user.access_token}`,
                        Accept: 'application/json'
                    }
                })
                .then((res) => {
                    postData(res.data);
                    navigate("/")
                })
                .catch((err) => console.log(err));
        }
    }, [user]);

    const postData = async (data: any) => {
        try {
            const requestBody = {
                name: data.name,
                email: data.email,
                avatar: data.picture,
            }

            const response = await axios.post('http://localhost:8080/api/v1/auth/google',
                requestBody
            );

            const dataRes = response.data.token;
            localStorage.setItem('token', dataRes);
        } catch (error) {
            console.log(error);
        }
    }

    return (
        <div>
            <div className="text-right">
                <a 
                    className="cursor-pointer"
                    onClick={handleForgetClick}
                    >Forget password?</a>
            </div>
            <p className="mt-7 text-neutral-400">----------- Sign Up using -----------</p>
            <div
                className="mt-7 flex justify-center cursor-pointer">
                    <FcGoogle size={40} onClick={() => login()} />
            </div>
            <div className="mt-4 ">
                <p>Don't have account, <a 
                    className="cursor-pointer"
                    onClick={handleRegisterClick}>
                        register?
                    </a>
                </p>
            </div>
        </div>
    );
}