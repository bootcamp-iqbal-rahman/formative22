import { DetailMenu } from "../atoms/DetailMenu";

export const DetailList: React.FC = () => {
    return (
        <div className="bg-3 rounded-lg shadow-lg">
            <div className="p-2 lg:p-5 lg:pt-2.5 text-left">
                <DetailMenu />
            </div>
        </div>
    );
}