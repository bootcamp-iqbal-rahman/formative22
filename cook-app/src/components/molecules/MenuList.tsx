import { useEffect, useState } from "react";
import { Menu } from "../atoms/Menu";
import axios from "axios";
import { useNavigate } from "react-router-dom";

export const MenuList: React.FC = () => {
    const [suggest, setSuggest] = useState<MenuComponent[]>([]);
    const navigate = useNavigate();

    useEffect(() => {
        const fetchProducts = async () => {
            const search = generateRandomLetter();
            const { data } = await axios.get("http://localhost:8080/api/data/menu/title/all?search=" + search);
            const limitedData = data.slice(0, 5);
            setSuggest(limitedData);
        };

        fetchProducts();
    }, []); 

    function generateRandomLetter(): string {
        const randomNumber: number = Math.floor(Math.random() * 26);
        const randomLetter: string = String.fromCharCode(97 + randomNumber); // 97 is ASCII code for 'a'
        return randomLetter;
    }

    const handleSuggestClick = (list: any) => {
        navigate("/detail/" + list);
    }

    return (
        <div className="bg-3 rounded-lg shadow-lg">
            <div className="p-2 lg:p-5 lg:pt-2.5 text-left">
                <h1 className="text-lg font-black">Suggestion menu</h1>
                {suggest.map((suggest, index) => (
                    <Menu 
                        key={index}
                        onClick={() => handleSuggestClick(suggest.id)}
                        title={suggest.name}
                        desc="Mama budi" />
                ))}
            </div>
        </div>
    );
}
