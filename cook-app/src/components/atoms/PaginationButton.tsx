interface Component {
    onClick: () => void;
    currentPage: number;
    maxPages: number;
    text: string;
}

export const PaginationButton: React.FC<Component> = (props) => {
    return (
        <button 
            onClick={props.onClick}  
            disabled={props.currentPage === props.maxPages}>
            {props.text}
        </button>
    );
}