export const DetailMenu: React.FC = () => {
    return (
        <div className="mt-2">
            <div className="">
                <button className="bg-5 hover:bg-4 text-white font-bold py-2 px-4 rounded-lg min-w-full">
                        Bookmark
                </button>
            </div>

            <div className="mt-2 ">
                <button className="bg-5 hover:bg-4 text-white font-bold py-2 px-4 rounded-lg min-w-full">
                        Share
                </button>
            </div>

        </div>
    );
}