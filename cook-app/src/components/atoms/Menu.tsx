interface Component {
    title: string;
    desc: string;
    onClick: (list: string) => void;
}

export const Menu: React.FC<Component> = (props) => {
    return (
        <div className="mt-2 cursor-pointer"
            onClick={() => props.onClick(props.title)}
        >
            <h1 className="text-sm font-semibold">{props.title}</h1>
            <p className="text-xs font">{props.desc}</p>
        </div>
    );
}