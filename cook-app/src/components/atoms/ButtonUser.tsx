import React from "react";

interface FieldComponent {
    text: string;
    onClick: () => void;
}

export const ButtonUser: React.FC<FieldComponent> = (props) => {
    return (
        <button 
            onClick={props.onClick}
            className="button-form text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full px-5 py-2.5 text-center"
        >{props.text}</button>
    );
}