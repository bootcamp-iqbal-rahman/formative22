import React from "react";
import { CgProfile } from "react-icons/cg";

export const NavbarTop: React.FC = () => {
    return (
        <div className="flex w-full items-center min-h-20 bg-4 drop-shadow-md">
            <h1 className="ml-8 lg:ml-20 flex-1 text-left text-4xl font-bold text-white"><a href="/">CookRec</a></h1>
            <div className="mr-8 lg:mr-20 flex flex-1 justify-end text-right cursor-pointer">
                <a href="/login"><CgProfile size={40} color="white" /></a>
            </div>
        </div>
    )
}