import { CiSearch } from "react-icons/ci";

export const SearchBar: React.FC<Search> = (props) => {
    return (
        <div className="flex bg-3 rounded-3xl shadow-lg min-h-10 items-center">
            <div className="p-3">
                <CiSearch />
            </div>
            <input 
                className="w-full bg-transparent border-none outline-none" 
                placeholder="Search menu ..."
                value={props.value}
                onChange={props.onChange}
                ></input>
        </div>
    );
}