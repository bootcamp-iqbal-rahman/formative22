import { CgProfile } from "react-icons/cg";

interface Component {
    name: string;
    description: string;
    username: string;
    date: string;
}

export const DetailCard: React.FC<Component> = (props) => {
    return (
        <div className="w-full bg-2 rounded-2xl shadow-md">
            <div className="p-6">
                <div className="text-left">
                    <h1 className="text-lg font-black">{props.name}</h1>
                    <h5 className="text-xs mt-2">{props.description}</h5>
                </div>
                <div className="grid grid-cols-1 sm:grid-cols-2 items-center mt-4">
                    <div className="text-left flex gap-3">
                        <CgProfile size={35} />
                        <div>
                            <h6 className="text-sm">{props.username}</h6>
                            <p className="text-xs font-medium">{props.date}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}