import { CgProfile } from "react-icons/cg";
import { useNavigate } from "react-router-dom";

export const Card: React.FC<CardComponent> = (props) => {
    const navigate = useNavigate();

    const handleDetailClick = () => {
        navigate("/detail/" + props.id);

    }
    
    return (
        <div className="mt-10 w-full bg-2 rounded-2xl shadow-md">
            <div className="bg-black rounded-lg">
                <img className="rounded-t-lg" 
                    src={props.image}
                ></img>
            </div>
            <div className="p-6">
                <div className="text-left">
                    <h1 className="text-lg font-black">{props.title}</h1>
                    <h5 className="text-xs mt-2">{props.ingredients}</h5>
                </div>
                <div className="grid grid-cols-1 sm:grid-cols-2 items-center mt-4">
                    <div className="text-left flex gap-3">
                        <CgProfile size={35} />
                        <div>
                            <h6 className="text-sm">{props.username}</h6>
                            <p className="text-xs font-medium">{props.date}</p>
                        </div>
                    </div>
                    <div className="flex justify-center mt-4 sm:mt-0 sm:justify-end">
                        <button 
                            onClick={() => handleDetailClick()}
                            className="bg-5 hover:bg-4 text-white font-bold py-2 px-4 rounded-full">
                            Detail Menu
                        </button>
                    </div>
                </div>
            </div>
        </div>
    );
}