type DetailItemType = IngredientsComponent | StepsComponent;

interface Component {
    title: string;
    ingredients: DetailItemType[];
}

function isIngredientsComponent(item: DetailItemType): item is IngredientsComponent {
    return 'amount' in item && 'unit' in item && 'name' in item;
}

export const DetailInfo: React.FC<Component> = (props) => {
    return (
        <div className="mt-10 w-full bg-2 rounded-2xl shadow-md">
            <div className="p-6">
                <div className="text-left">
                    <h1 className="text-lg font-black">{props.title}</h1>
                    {props.ingredients.map((item, index) => (
                        <h5 key={index} className="text-xs mt-2">
                            {isIngredientsComponent(item) ? (
                                <span className="font-bold">{item.amount} {item.unit} </span>
                            ) : (
                                <h5 className="font-bold text-sm"> {item.name}</h5>
                            )}
                            {isIngredientsComponent(item) ? item.name : item.description}
                        </h5>
                    ))}
                </div>
            </div>
        </div>
    );
}