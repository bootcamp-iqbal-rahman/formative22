export const Footer: React.FC = () => {
    return (
        <div className="min-h-28 sm:min-h-36 bg-1 mt-10 grid grid-cols-3 sm:grid-cols-2 items-center">
            <div className="col-span-1">
                <h1 className="ml-8 lg:ml-20 flex-1 text-left text-xl sm:text-4xl font-bold text-black">CookRec</h1>
            </div>
            <div className="col-span-2 sm:col-span-1 space-x-2 sm:space-x-6">
                <a>About</a>
                <a>Contribute</a>
                <a>Contact Us</a>
            </div>
        </div>
    );
}