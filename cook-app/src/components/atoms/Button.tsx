import React from "react";

interface FieldComponent {
    type: "button" | "submit" | "reset";
    text: string;
}

export const Button: React.FC<FieldComponent> = (props) => {
    return (
        <button 
            type={props.type} 
            className="button-form text-white bg-5 hover:bg-4 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full px-5 py-2.5 text-center"
        >{props.text}</button>
    );
}